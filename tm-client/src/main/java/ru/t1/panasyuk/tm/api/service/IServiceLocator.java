package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.panasyuk.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IUserEndpoint getUserEndpoint();

}