package ru.t1.panasyuk.tm.endpoint;

import io.qameta.allure.junit4.DisplayName;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.*;
import org.junit.experimental.categories.Category;
import ru.t1.panasyuk.tm.api.endpoint.IAuthEndpoint;
import ru.t1.panasyuk.tm.api.endpoint.IProjectEndpoint;
import ru.t1.panasyuk.tm.api.endpoint.ITaskEndpoint;
import ru.t1.panasyuk.tm.api.endpoint.IUserEndpoint;
import ru.t1.panasyuk.tm.api.service.IPropertyService;
import ru.t1.panasyuk.tm.dto.request.project.ProjectClearRequest;
import ru.t1.panasyuk.tm.dto.request.project.ProjectCreateRequest;
import ru.t1.panasyuk.tm.dto.request.task.*;
import ru.t1.panasyuk.tm.dto.request.user.UserLoginRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserRegistryRequest;
import ru.t1.panasyuk.tm.dto.request.user.UserRemoveRequest;
import ru.t1.panasyuk.tm.dto.response.task.TaskCreateResponse;
import ru.t1.panasyuk.tm.dto.response.task.TaskFindAllByProjectIdResponse;
import ru.t1.panasyuk.tm.dto.response.task.TaskFindOneByIndexResponse;
import ru.t1.panasyuk.tm.dto.response.task.TaskListResponse;
import ru.t1.panasyuk.tm.dto.response.user.UserLoginResponse;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.marker.SoapCategory;
import ru.t1.panasyuk.tm.model.Task;
import ru.t1.panasyuk.tm.service.PropertyService;

import java.util.List;

@Category(SoapCategory.class)
@DisplayName("Тестирование эндпоинта Task")
public class TaskEndpointTest {

    @NotNull
    private static IUserEndpoint userEndpoint;

    @Nullable
    private static String adminToken;

    @Nullable
    private static String userToken;

    @NotNull
    private static IProjectEndpoint projectEndpoint;

    @NotNull
    private static ITaskEndpoint taskEndpoint;

    @NotNull
    private static IPropertyService propertyService;

    @NotNull
    private String projectId;

    @NotNull
    private String firstTaskId;

    @NotNull
    private String secondTaskId;

    @BeforeClass
    public static void initUser() {
        propertyService = new PropertyService();
        projectEndpoint = IProjectEndpoint.newInstance(propertyService);
        taskEndpoint = ITaskEndpoint.newInstance(propertyService);
        @NotNull final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService);
        userEndpoint = IUserEndpoint.newInstance(propertyService);
        @NotNull UserLoginRequest loginRequest = new UserLoginRequest("SADMIN", "SADMIN");
        @NotNull UserLoginResponse response = authEndpoint.loginUser(loginRequest);
        adminToken = response.getToken();

        @NotNull final UserRegistryRequest registryRequest = new UserRegistryRequest("TEST", "TEST", "TEST@TEST");
        userEndpoint.registryUser(registryRequest);
        loginRequest = new UserLoginRequest("TEST", "TEST");
        response = authEndpoint.loginUser(loginRequest);
        userToken = response.getToken();
    }

    @AfterClass
    public static void dropUser() {
        @NotNull final UserRemoveRequest removeRequest = new UserRemoveRequest(adminToken, "TEST");
        userEndpoint.removeUser(removeRequest);
    }

    @Before
    public void initData() {
        @NotNull final ProjectCreateRequest createRequest = new ProjectCreateRequest(userToken, "Test Project 1", "Desc");
        projectId = projectEndpoint.createProject(createRequest).getProject().getId();
        @NotNull TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, "Test Task 1", "Desc");
        @Nullable final Task firstTask = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(firstTask);
        firstTaskId = firstTask.getId();
        @NotNull final TaskBindToProjectRequest bindRequest = new TaskBindToProjectRequest(userToken, projectId, firstTaskId);
        taskEndpoint.bindTaskToProject(bindRequest);
        taskCreateRequest = new TaskCreateRequest(userToken, "Test Task 2", "Desc");
        @Nullable final Task secondTask = taskEndpoint.createTask(taskCreateRequest).getTask();
        Assert.assertNotNull(secondTask);
        secondTaskId = secondTask.getId();
        @NotNull final TaskBindToProjectRequest bindRequest2 = new TaskBindToProjectRequest(userToken, projectId, secondTaskId);
        taskEndpoint.bindTaskToProject(bindRequest2);
    }

    @After
    public void dropData() {
        @NotNull final ProjectClearRequest clearRequest = new ProjectClearRequest(userToken);
        projectEndpoint.clearProject(clearRequest);
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        taskEndpoint.clearTask(request);
    }

    @Nullable
    private Task findTaskById(@NotNull final String taskId) {
        @NotNull final TaskFindOneByIdRequest request = new TaskFindOneByIdRequest(userToken, taskId);
        return taskEndpoint.findTaskById(request).getTask();
    }

    @Nullable
    private Task findTaskByIndex(@NotNull final Integer index) {
        @NotNull final TaskFindOneByIndexRequest request = new TaskFindOneByIndexRequest(userToken, index);
        return taskEndpoint.findTaskByIndex(request).getTask();
    }

    @Test
    @DisplayName("Привязывание задачи к проекту")
    public void bindToProjectTest() {
        @NotNull TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, "Test Task 1", "Desc");
        @NotNull final String taskId = taskEndpoint.createTask(taskCreateRequest).getTask().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(userToken, projectId, taskId);
        Assert.assertNotNull(taskEndpoint.bindTaskToProject(request));
        @Nullable final Task boundTask = findTaskById(taskId);
        Assert.assertNotNull(boundTask);
        Assert.assertEquals(boundTask.getProjectId(), projectId);
    }

    @Test(expected = Exception.class)
    @DisplayName("Привязывание задачи к проекту без токена")
    public void bindToProjectTestNegative() {
        @NotNull TaskCreateRequest taskCreateRequest = new TaskCreateRequest(userToken, "Test Task 1", "Desc");
        @NotNull final String taskId = taskEndpoint.createTask(taskCreateRequest).getTask().getId();
        @NotNull final TaskBindToProjectRequest request = new TaskBindToProjectRequest(null, projectId, taskId);
        taskEndpoint.bindTaskToProject(request);
    }

    @Test
    @DisplayName("Изменение статуса задачи по Id")
    public void changeStatusByIdTest() {
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(userToken, firstTaskId, status);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusById(request));
        @Nullable final Task task = findTaskById(firstTaskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса задачи по Id без токена")
    public void changeStatusByIdTestNegative() {
        @NotNull final TaskChangeStatusByIdRequest request = new TaskChangeStatusByIdRequest(null, firstTaskId, Status.IN_PROGRESS);
        taskEndpoint.changeTaskStatusById(request);
    }

    @Test
    @DisplayName("Изменение статуса задачи по индексу")
    public void changeStatusByIndexTest() {
        @NotNull final Status status = Status.IN_PROGRESS;
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(userToken, 1, status);
        Assert.assertNotNull(taskEndpoint.changeTaskStatusByIndex(request));
        @Nullable final Task task = findTaskByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertEquals(status, task.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса задачи по индексу без токена")
    public void changeStatusByIndexTestNegative() {
        @NotNull final TaskChangeStatusByIndexRequest request = new TaskChangeStatusByIndexRequest(null, 0, Status.IN_PROGRESS);
        taskEndpoint.changeTaskStatusByIndex(request);
    }

    @Test
    @DisplayName("Удаление задач")
    public void clearTest() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(userToken);
        Assert.assertNotNull(taskEndpoint.clearTask(request));
        @NotNull final TaskListRequest listRequest = new TaskListRequest(userToken, Sort.BY_NAME);
        @Nullable final List<Task> tasks = taskEndpoint.listTask(listRequest).getTasks();
        Assert.assertNull(tasks);
    }

    @Test(expected = Exception.class)
    @DisplayName("Удаление задач без токена")
    public void clearTestNegative() {
        @NotNull final TaskClearRequest request = new TaskClearRequest(null);
        taskEndpoint.clearTask(request);
    }

    @Test
    @DisplayName("Изменение статуса задачи на Completed по Id")
    public void completeByIdTest() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(userToken, secondTaskId);
        Assert.assertNotNull(taskEndpoint.completeTaskById(request));
        @Nullable final Task task = findTaskById(secondTaskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса задачи на Completed по Id без токена")
    public void completeByIdTestNegative() {
        @NotNull final TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(null, secondTaskId);
        taskEndpoint.completeTaskById(request);
    }

    @Test
    @DisplayName("Изменение статуса задачи на Completed по индексу")
    public void completeByIndexTest() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(userToken, 1);
        Assert.assertNotNull(taskEndpoint.completeTaskByIndex(request));
        @Nullable final Task task = findTaskByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.COMPLETED, task.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса задачи на Completed по индексу без токена")
    public void completeByIndexTestNegative() {
        @NotNull final TaskCompleteByIndexRequest request = new TaskCompleteByIndexRequest(null, 1);
        taskEndpoint.completeTaskByIndex(request);
    }

    @Test
    @DisplayName("Создание задачи")
    public void createTest() {
        @NotNull final String name = "NAME TEST CREATE";
        @NotNull final String desc = "DESC TEST CREATE";
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(userToken, name, desc);
        @Nullable final TaskCreateResponse response = taskEndpoint.createTask(request);
        Assert.assertNotNull(response);
        @Nullable final Task returnedTask = response.getTask();
        Assert.assertNotNull(returnedTask);
        @NotNull final String taskId = returnedTask.getId();
        @Nullable final Task createdTask = findTaskById(taskId);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(name, createdTask.getName());
        Assert.assertEquals(desc, createdTask.getDescription());
    }

    @Test(expected = Exception.class)
    @DisplayName("Создание задачи без токена")
    public void createTestNegative() {
        @NotNull final TaskCreateRequest request = new TaskCreateRequest(null, "NAME", "DESC");
        taskEndpoint.createTask(request);
    }

    @Test
    @DisplayName("Поиск задачи по Id проекта")
    public void findAllByProjectIdTest() {
        @NotNull final TaskFindAllByProjectIdRequest request = new TaskFindAllByProjectIdRequest(userToken, projectId);
        @Nullable final TaskFindAllByProjectIdResponse response = taskEndpoint.findAllByProjectId(request);
        Assert.assertNotNull(response);
        @Nullable final List<Task> tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertEquals(2, tasks.size());
    }

    @Test(expected = Exception.class)
    @DisplayName("Поиск задачи по Id проекта без токена")
    public void findAllByProjectIdTestNegative() {
        @NotNull final TaskFindAllByProjectIdRequest request = new TaskFindAllByProjectIdRequest(null, projectId);
        taskEndpoint.findAllByProjectId(request);
    }

    @Test
    @DisplayName("Поиск задачи по Id")
    public void findOneByIdTest() {
        @NotNull final TaskFindOneByIdRequest request = new TaskFindOneByIdRequest(userToken, firstTaskId);
        @Nullable final Task task = taskEndpoint.findTaskById(request).getTask();
        Assert.assertNotNull(task);
    }

    @Test(expected = Exception.class)
    @DisplayName("Поиск задачи по Id без токена")
    public void findOneByIdTestNegative() {
        @NotNull final TaskFindOneByIdRequest request = new TaskFindOneByIdRequest(null, firstTaskId);
        taskEndpoint.findTaskById(request);
    }

    @Test
    @DisplayName("Поиск задачи по индексу")
    public void findOneByIndexTest() {
        @NotNull final TaskFindOneByIndexRequest request = new TaskFindOneByIndexRequest(userToken, 1);
        TaskFindOneByIndexResponse response = taskEndpoint.findTaskByIndex(request);
        @Nullable final Task task = response.getTask();
        Assert.assertNotNull(task);
    }

    @Test(expected = Exception.class)
    @DisplayName("Поиск задачи по индексу без токена")
    public void findOneByIndexTestNegative() {
        @NotNull final TaskFindOneByIndexRequest request = new TaskFindOneByIndexRequest(null, 0);
        taskEndpoint.findTaskByIndex(request);
    }

    @Test
    @DisplayName("Получение списка задач")
    public void listTest() {
        @NotNull final TaskListRequest request = new TaskListRequest(userToken, null);
        @Nullable final TaskListResponse response = taskEndpoint.listTask(request);
        Assert.assertNotNull(response);
        @Nullable final List<Task> tasks = response.getTasks();
        Assert.assertNotNull(tasks);
        Assert.assertTrue(tasks.size() > 0);
        Assert.assertEquals(firstTaskId, tasks.get(0).getId());
        Assert.assertEquals(secondTaskId, tasks.get(1).getId());
    }

    @Test(expected = Exception.class)
    @DisplayName("Получение списка задач без токена")
    public void listTestNegative() {
        @NotNull final TaskListRequest request = new TaskListRequest(null, Sort.BY_NAME);
        taskEndpoint.listTask(request);
    }

    @Test
    @DisplayName("Удаление задачи по Id")
    public void removeByIdTest() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(userToken, firstTaskId);
        Assert.assertNotNull(taskEndpoint.removeTaskById(request));
        @Nullable final Task task = findTaskById(firstTaskId);
        Assert.assertNull(task);
    }

    @Test(expected = Exception.class)
    @DisplayName("Удаление задачи по Id без токена")
    public void removeByIdTestNegative() {
        @NotNull final TaskRemoveByIdRequest request = new TaskRemoveByIdRequest(null, firstTaskId);
        taskEndpoint.removeTaskById(request);
    }

    @Test
    @DisplayName("Удаление задачи по индексу")
    public void removeByIndexTest() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(userToken, 1);
        Assert.assertNotNull(taskEndpoint.removeTaskByIndex(request));
        @Nullable final Task task = findTaskByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertNotEquals(firstTaskId, task.getId());
    }

    @Test(expected = Exception.class)
    @DisplayName("Удаление задачи по индексу без токена")
    public void removeByIndexTestNegative() {
        @NotNull final TaskRemoveByIndexRequest request = new TaskRemoveByIndexRequest(null, 0);
        taskEndpoint.removeTaskByIndex(request);
    }

    @Test
    @DisplayName("Изменение статуса задачи на In Progress по Id")
    public void startByIdTest() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(userToken, secondTaskId);
        Assert.assertNotNull(taskEndpoint.startTaskById(request));
        @Nullable final Task task = findTaskById(secondTaskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса задачи на In Progress по Id без токена")
    public void startByIdTestNegative() {
        @NotNull final TaskStartByIdRequest request = new TaskStartByIdRequest(null, secondTaskId);
        taskEndpoint.startTaskById(request);
    }

    @Test
    @DisplayName("Изменение статуса задачи на In Progress по индексу")
    public void startByIndexTest() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(userToken, 1);
        Assert.assertNotNull(taskEndpoint.startTaskByIndex(request));
        @Nullable final Task task = findTaskByIndex(1);
        Assert.assertNotNull(task);
        Assert.assertEquals(Status.IN_PROGRESS, task.getStatus());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение статуса задачи на In Progress по индексу без токена")
    public void startByIndexTestNegative() {
        @NotNull final TaskStartByIndexRequest request = new TaskStartByIndexRequest(null, 1);
        taskEndpoint.startTaskByIndex(request);
    }

    @Test
    @DisplayName("Отвязывание задачи от проекта")
    public void unbindFromProjectTest() {
        @NotNull final TaskUnbindFromProjectRequest request = new TaskUnbindFromProjectRequest(userToken, firstTaskId, projectId);
        Assert.assertNotNull(taskEndpoint.unbindTaskFromProject(request));
        @Nullable final Task unboundTask = findTaskById(firstTaskId);
        Assert.assertNotNull(unboundTask);
        Assert.assertNotEquals(unboundTask.getProjectId(), projectId);
    }

    @Test
    @DisplayName("Изменение задачи по Id")
    public void updateByIdTest() {
        @NotNull final String name = "NAME TEST UPDATE";
        @NotNull final String desc = "DESC TEST UPDATE";
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(userToken, firstTaskId, name, desc);
        Assert.assertNotNull(taskEndpoint.updateTaskById(request));
        @Nullable final Task updatedTask = findTaskById(firstTaskId);
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(name, updatedTask.getName());
        Assert.assertEquals(desc, updatedTask.getDescription());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение задачи по Id без токена")
    public void updateByIdTestNegative() {
        @NotNull final TaskUpdateByIdRequest request = new TaskUpdateByIdRequest(null, firstTaskId, "NAME", "DESC");
        taskEndpoint.updateTaskById(request);
    }

    @Test
    @DisplayName("Изменение задачи по индексу")
    public void updateByIndexTest() {
        @NotNull final String name = "NAME TEST UPDATE";
        @NotNull final String desc = "DESC TEST UPDATE";
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(userToken, 1, name, desc);
        Assert.assertNotNull(taskEndpoint.updateTaskByIndex(request));
        @Nullable final Task updatedTask = findTaskByIndex(1);
        Assert.assertNotNull(updatedTask);
        Assert.assertEquals(name, updatedTask.getName());
        Assert.assertEquals(desc, updatedTask.getDescription());
    }

    @Test(expected = Exception.class)
    @DisplayName("Изменение задачи по индексу без токена")
    public void updateByIndexTestNegative() {
        @NotNull final TaskUpdateByIndexRequest request = new TaskUpdateByIndexRequest(null, 0, "NAME", "DESC");
        taskEndpoint.updateTaskByIndex(request);
    }

}