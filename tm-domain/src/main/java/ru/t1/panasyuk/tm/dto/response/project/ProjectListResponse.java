package ru.t1.panasyuk.tm.dto.response.project;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Project;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListResponse extends AbstractProjectResponse {

    @Nullable
    private List<Project> projects;

    public ProjectListResponse(@NotNull final List<Project> projects) {
        this.projects = projects;
    }

    public ProjectListResponse(@NotNull final Throwable throwable) {
        super(throwable);
    }

}