package ru.t1.panasyuk.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Role;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
public final class Session extends AbstractUserOwnedModel {

    @Nullable
    private Role role = null;

    @Override
    public int hashCode() {
        return getId().hashCode();
    }

    @Override
    public boolean equals(@NotNull final Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Session)) return false;
        @NotNull final Session task = (Session) obj;
        return task.getId().equals(this.getId());
    }

}