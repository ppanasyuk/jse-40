package ru.t1.panasyuk.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Task;

import java.util.List;

@CacheNamespace
public interface ITaskRepository {

    @Insert("INSERT INTO tm_task (id, name, created, status, description, project_id, user_id) " +
            "VALUES (#{id}, #{name}, #{created}, #{status}, #{description}, #{projectId}, #{userId})")
    void add(@NotNull Task task);

    @Insert("INSERT INTO tm_task (id, name, created, status, description, project_id, user_id) " +
            "VALUES (#{id}, #{name}, #{created}, #{status}, #{description}, #{projectId}, #{userId})")
    void addForUser(@NotNull @Param("userId") String userId, @NotNull Task task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId}")
    void clearForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_task")
    void clear();

    @Select("SELECT * FROM tm_task ORDER BY created DESC")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<Task> findAll();

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created DESC")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<Task> findAllForUser(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId} ORDER BY created DESC")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<Task> findAllByProjectId(
            @NotNull @Param("userId") String userId,
            @Nullable @Param("projectId") String projectId
    );

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<Task> findAllOrderByCreated(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY name")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<Task> findAllOrderByName(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY status")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable List<Task> findAllOrderByStatus(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneById(@NotNull @Param("userId") String userId, @Nullable @Param("id") String id);

    @Select("SELECT * FROM tm_task WHERE user_id = #{userId} ORDER BY created DESC LIMIT 1 OFFSET #{index}-1")
    @Results(value = {
            @Result(property = "userId", column = "user_id"),
            @Result(property = "projectId", column = "project_id")
    })
    @Nullable Task findOneByIndex(@NotNull @Param("userId") String userId, @Nullable @Param("index") Integer index);

    @Select("SELECT COUNT(id) FROM tm_task WHERE user_id = #{userId}")
    int getSize(@NotNull String userId);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND id = #{id}")
    void remove(@Nullable Task task);

    @Delete("DELETE FROM tm_task WHERE user_id = #{userId} AND project_id = #{projectId}")
    void removeAllByProjectId(@NotNull @Param("userId") String userId, @Nullable @Param("projectId") String projectId);

    @Update("UPDATE tm_task SET name = #{name}, description = #{description}, status = #{status}, project_id = #{projectId} " +
            "WHERE user_id = #{userId} AND id = #{id}")
    void update(@NotNull final Task task);

}