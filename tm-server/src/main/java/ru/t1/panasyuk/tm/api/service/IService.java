package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.model.AbstractModel;

import java.util.List;

public interface IService<M extends AbstractModel> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

}