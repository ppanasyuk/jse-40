package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.enumerated.Role;
import ru.t1.panasyuk.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    User add(@Nullable User user);

    void clear();

    @NotNull
    User create(@NotNull String login, @NotNull String password) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @Nullable String email) throws Exception;

    @NotNull
    User create(@NotNull String login, @NotNull String password, @NotNull Role role) throws Exception;

    boolean existsById(@Nullable String id);

    List<User> findAll();

    User findByLogin(@Nullable String login) throws Exception;

    User findByEmail(@Nullable String email) throws Exception;

    User findOneById(@Nullable String id);

    User findOneByIndex(@Nullable Integer index);

    int getSize();

    Boolean isLoginExist(@Nullable String login) throws Exception;

    Boolean isEmailExist(@Nullable String email) throws Exception;

    User removeById(@Nullable String id);

    void lockUserByLogin(@Nullable String login) throws Exception;

    User remove(@Nullable User user) throws Exception;

    User removeByIndex(@Nullable Integer index);

    @NotNull
    User removeByLogin(@Nullable String login) throws Exception;

    @NotNull
    User removeByEmail(@Nullable String email) throws Exception;

    Collection<User> set(@NotNull Collection<User> users);

    @NotNull
    User setPassword(@Nullable String id, @Nullable String password) throws Exception;

    void unlockUserByLogin(@Nullable String login) throws Exception;

    void update(@NotNull User user);

    @NotNull
    User updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

}