package ru.t1.panasyuk.tm.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.ISessionService;
import ru.t1.panasyuk.tm.exception.entity.EntityNotFoundException;
import ru.t1.panasyuk.tm.exception.field.IdEmptyException;
import ru.t1.panasyuk.tm.exception.field.IndexIncorrectException;
import ru.t1.panasyuk.tm.model.Session;

import java.util.List;

public final class SessionService implements ISessionService {

    @NotNull
    final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public Session add(@NotNull final String userId, @Nullable final Session session) {
        if (session == null) return null;
        session.setUserId(userId);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                repository.add(session);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return session;
    }

    public void clear(@NotNull final String userId) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                repository.clearForUser(userId);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
    }

    @Override
    public boolean existsById(@NotNull final String userId, @Nullable final String id) {
        boolean result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                result = repository.findOneById(userId, id) != null;
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @Nullable
    @Override
    public List<Session> findAll() {
        @Nullable final List<Session> models;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                models = repository.findAll();
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return models;
    }

    @Nullable
    @Override
    public List<Session> findAll(@NotNull final String userId) {
        @Nullable final List<Session> models;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                models = repository.findAllForUser(userId);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return models;
    }

    @Nullable
    @Override
    public Session findOneById(@NotNull final String userId, @Nullable final String id) {
        if (id == null) return null;
        @Nullable final Session session;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                session = repository.findOneById(userId, id);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return session;
    }

    @Nullable
    @Override
    public Session findOneByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Session session;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                session = repository.findOneByIndex(userId, index);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return session;
    }

    @Override
    public int getSize(@NotNull final String userId) {
        int result;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                result = repository.getSize(userId);
            } catch (@NotNull final Exception e) {
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return result;
    }

    @NotNull
    @Override
    public Session remove(@Nullable final Session session) {
        if (session == null) throw new EntityNotFoundException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                repository.remove(session);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return session;
    }

    @NotNull
    @Override
    public Session removeById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final Session removedSession;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                removedSession = repository.findOneById(userId, id);
                if (removedSession == null) throw new EntityNotFoundException();
                repository.remove(removedSession);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedSession;
    }

    @NotNull
    @Override
    public Session removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new IndexIncorrectException();
        if (index > getSize(userId)) throw new IndexIncorrectException();
        @Nullable final Session removedSession;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            try {
                @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
                removedSession = repository.findOneByIndex(userId, index);
                if (removedSession == null) throw new EntityNotFoundException();
                repository.remove(removedSession);
                sqlSession.commit();
            } catch (@NotNull final Exception e) {
                sqlSession.rollback();
                throw e;
            }
        } catch (@NotNull final Exception e) {
            throw e;
        }
        return removedSession;
    }

}