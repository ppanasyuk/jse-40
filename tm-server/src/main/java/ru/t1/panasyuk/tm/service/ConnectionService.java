package ru.t1.panasyuk.tm.service;

import lombok.SneakyThrows;
import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.ibatis.datasource.pooled.PooledDataSource;
import org.apache.ibatis.logging.LogFactory;
import org.apache.ibatis.mapping.Environment;
import org.apache.ibatis.session.Configuration;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.apache.ibatis.transaction.TransactionFactory;
import org.apache.ibatis.transaction.jdbc.JdbcTransactionFactory;
import org.apache.xpath.operations.Bool;
import org.jetbrains.annotations.NotNull;
import org.mybatis.caches.hazelcast.LoggingHazelcastCache;
import ru.t1.panasyuk.tm.api.repository.IProjectRepository;
import ru.t1.panasyuk.tm.api.repository.ISessionRepository;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.api.repository.IUserRepository;
import ru.t1.panasyuk.tm.api.service.IConnectionService;
import ru.t1.panasyuk.tm.api.service.IPropertyService;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.DriverManager;

public final class ConnectionService implements IConnectionService {

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final SqlSessionFactory sqlSessionFactory;

    public ConnectionService(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.sqlSessionFactory = getSqlSessionFactory();
    }

    @NotNull
    @Override
    public SqlSession getSqlSession() {
        return sqlSessionFactory.openSession();
    }

    private SqlSessionFactory getSqlSessionFactory() {
        @NotNull final Boolean loggingEnabled = propertyService.getDBLoggingEnabled();
        if (loggingEnabled) LogFactory.useStdOutLogging();
        @NotNull final String username = propertyService.getDBUser();
        @NotNull final String password = propertyService.getDBPassword();
        @NotNull final String url = propertyService.getDBUrl();
        @NotNull final String driver = propertyService.getDBDriver();
        @NotNull final Boolean secondLevelCacheEnabled = propertyService.getDBSecondLevelCacheEnabled();
        @NotNull final DataSource dataSource = new PooledDataSource(driver, url, username, password);
        @NotNull final TransactionFactory transactionFactory = new JdbcTransactionFactory();
        @NotNull final Environment environment = new Environment("tm", transactionFactory, dataSource);
        @NotNull final Configuration configuration = new Configuration(environment);
        configuration.setCacheEnabled(secondLevelCacheEnabled);
        configuration.addCache(new LoggingHazelcastCache("hz"));
        configuration.addMapper(IProjectRepository.class);
        configuration.addMapper(ITaskRepository.class);
        configuration.addMapper(ISessionRepository.class);
        configuration.addMapper(IUserRepository.class);
        return new SqlSessionFactoryBuilder().build(configuration);
    }

}