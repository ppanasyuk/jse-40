package ru.t1.panasyuk.tm.api.repository;

import org.apache.ibatis.annotations.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Session;

import java.util.List;

@CacheNamespace
public interface ISessionRepository {

    @Insert("INSERT INTO tm_session (id, created, role, user_id) " +
            "VALUES (#{id}, #{created}, #{role}, #{userId})")
    void add(@NotNull Session session);

    @Insert("INSERT INTO tm_session (id, created, role, user_id) " +
            "VALUES (#{id}, #{created}, #{role}, #{userId})")
    void addForUser(@NotNull @Param("userId") String userId, @NotNull Session session);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId}")
    void clearForUser(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_project")
    void clear();

    @Select("SELECT * FROM tm_session ORDER BY created DESC")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Session> findAll();

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} ORDER BY created DESC")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable List<Session> findAllForUser(@NotNull @Param("userId") String userId);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} AND id = #{id} LIMIT 1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneById(@NotNull @Param("userId") String userId, @Nullable @Param("id") String id);

    @Select("SELECT * FROM tm_session WHERE user_id = #{userId} ORDER BY created DESC LIMIT 1 OFFSET #{index}-1")
    @Results(value = {
            @Result(property = "userId", column = "user_id")
    })
    @Nullable Session findOneByIndex(@NotNull @Param("userId") String userId, @Nullable @Param("index") Integer index);

    @Select("SELECT COUNT(id) FROM tm_session WHERE user_id = #{userId}")
    int getSize(@NotNull @Param("userId") String userId);

    @Delete("DELETE FROM tm_session WHERE user_id = #{userId} AND id = #{id}")
    void remove(@Nullable Session session);

    @Update("UPDATE tm_session SET role = #{role} " +
            "WHERE user_id = #{userId} AND id = #{id}")
    void update(@NotNull Session session);

}