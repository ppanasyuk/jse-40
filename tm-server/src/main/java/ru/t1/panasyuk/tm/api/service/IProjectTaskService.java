package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.model.Project;
import ru.t1.panasyuk.tm.model.Task;

public interface IProjectTaskService {

    @NotNull
    Task bindTaskToProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

    Project removeProjectById(@NotNull String userId, @Nullable String projectId) throws Exception;

    Project removeProjectByIndex(@NotNull String userId, @Nullable Integer index) throws Exception;

    void clearProjects(@NotNull String userId) throws Exception;

    @NotNull
    Task unbindTaskFromProject(@NotNull String userId, @Nullable String projectId, @Nullable String taskId) throws Exception;

}