package ru.t1.panasyuk.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.panasyuk.tm.api.repository.ITaskRepository;
import ru.t1.panasyuk.tm.enumerated.Sort;
import ru.t1.panasyuk.tm.enumerated.Status;
import ru.t1.panasyuk.tm.model.Task;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface ITaskService extends IUserOwnedService<Task> {

    @NotNull
    Task changeTaskStatusById(@NotNull String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Task changeTaskStatusByIndex(@NotNull String userId, @Nullable Integer index, @Nullable Status status);

    @NotNull
    Task create(@NotNull String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Task create(@NotNull String userId, @Nullable String name);

    void clear();

    List<Task> findAllByProjectId(@NotNull String userId, @Nullable String projectId);

    List<Task> findAll();

    List<Task> findAll(@NotNull String userId, @Nullable Comparator comparator);

    List<Task> findAll(@NotNull String userId, @Nullable Sort sort);

    void removeAllByProjectId(@NotNull String userId, @Nullable String projectId);

    Collection<Task> set(@NotNull Collection<Task> tasks);

    void update(@NotNull Task task);

    @NotNull
    Task updateById(@NotNull String userId, @Nullable String id, @Nullable String name, @Nullable String description) throws Exception;

    @NotNull
    Task updateByIndex(@NotNull String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) throws Exception;

}